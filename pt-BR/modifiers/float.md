---
description: Aplique a propriedade float em qualquer elemento, e em qualquer breakpoint com nossas classes modificadoras.
title: Float
nnp.date: 10/03/2019
nnp.topic: article
keywords: float, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

## Float

Aplique a propriedade float em qualquer elemento, e em qualquer breakpoint com nossas classes modificadoras. Elas aplicam ou desativam o float em um determinado elemento.
Para fazer com que um elemento flutue para esquerda, adicione `.nv-float-left` ou `.nv-float-right` para fazer com que o elemento flutue para a direita.

```html
<div class="nv-float-left">Flutuar para esquerda em todos os tamanhos de viewport</div><br>
<div class="nv-float-right">Flutuar para direita em todos os tamanhos de viewport</div><br>
<div class="nv-float-none">Não flutuar em todos os tamanhos de viewport</div>
```

Também é possível utilizar nossos mixins no SCSS.  

```scss
.elemento {
  @include float-left;
}
.outro-elemento {
  @include float-right;
}
.mais-um-elemento {
  @include float-none;
}
```

As variações responsivas também estão disponíveis para `.nv-float-{valor}`, veja a lista abaixo.

- .nv-float-left
- .nv-float-right
- .nv-float-none
- .nv-float-xsm-left
- .nv-float-xsm-right
- .nv-float-xsm-none
- .nv-float-sm-left
- .nv-float-sm-right
- .nv-float-sm-none
- .nv-float-md-left
- .nv-float-md-right
- .nv-float-md-none
- .nv-float-lg-left
- .nv-float-lg-right
- .nv-float-lg-none
- .nv-float-xl-left
- .nv-float-xl-right
- .nv-float-xl-none
- .nv-float-xxl-left
- .nv-float-xxl-right
- .nv-float-xxl-none
- .nv-float-xxxl-left
- .nv-float-xxxl-right
- .nv-float-xxxl-none
