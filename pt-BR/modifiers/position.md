---
description: Altere rapidamente a posição de elementos com nossas classes modificadoras.
title: Posição
nnp.date: 10/03/2019
nnp.topic: article
keywords: posição, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Posição
Altere rapidamente a posição de elementos com nossas classes modificadoras.  

Para alterar a posição de um elemento adicione a classe `.nv-position-{valor}`, onde valor poderá ser: `relative`, `absolute`, `fixed`, `static` e `sticky`.

```html
<div class="nv-position-static">...</div>
<div class="nv-position-relative">...</div>
<div class="nv-position-absolute">...</div>
<div class="nv-position-fixed">...</div>
<div class="nv-position-sticky">...</div>
```

## Topo fixo

Nós também fornecemos uma classe para que você fixe um elemento HTML no topo de sua página, basta adicionar a classe `.nv-fixed-top` ao elemento HTML.  

```html
<div class="nv-fixed-top">...</div>
```

Rodapé fixo
Você também pode fixar um elemento HTML no rodapé de sua página, adicionando nossa classe modificadora .nv-fixed-bottom.

```html
<div class="nv-fixed-bottom">...</div>
```

> NOTA: Ao utilizar as classes modificadoras .nv-fixed-top e .nv-fixed-bottom poderá ser necessário a adição de CSS personalizado para ajustar a posição do elemento de acordo com o seu projeto.
