---
description: Oculte de maneira simples as barras de rolagem de um elemento HTML e controle a maneira com o conteúdo excede um elemento. 
title: Barras de rolagem
nnp.date: 10/03/2019
nnp.topic: article
keywords: barras de rolagem, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Barras de rolagem

Oculte de maneira simples as barras de rolagem de um elemento HTML e controle a maneira com o conteúdo excede um elemento.  

Nós fornecemos duas classes modificadoras a `.nv-overflow-auto` e a `.nv-overflow-hidden`, elas tornam visíveis 0u ocultam as barras de rolagem respectivamente. As classes ativam e desativam as propriedades `overflow-x` e `overflow-y`, você não poderá ativar ou desativar apenas uma propriedade específica, temos planos de adicionar essa possibilidade em versões futuras.  

```html
<div class="nv-overflow-auto">...</div>
<div class="nv-overflow-hidden">...</div>
```
