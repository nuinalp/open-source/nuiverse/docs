---
description:Manipule facilmente a largura ou altura de um elemento HTML com nossas classes modificadoras.
title: Dimensionamentos
nnp.date: 10/03/2019
nnp.topic: article
keywords: dimensionamentos, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Dimensionamentos
Manipule facilmente a largura ou altura de um elemento HTML com nossas classes modificadoras.  

Nossas classes modificadoras de tamanhos são geradas a partir de nossa variável “mapa” scss $sizes em `src\core\src\style\utils\variables\_sizes.scss`.  

Nossas variações de tamanhos iniciam-se no 0 vão até 100 em incrementos de 5, para alterar a largura você deve adicionar a classe modificadora `.nv-w-{valor}` ou `.nv-h-{valor}` para altura, conforme exibido no exemplo abaixo.  

Você pode alterar esses valores para gerar variações que atendam suas necessidades.  

```html
<div class="nv-w-5 nv-p-3">Largura 5%</div>
<div class="nv-w-10 nv-p-3">Largura 10%</div>
<div class="nv-w-15 nv-p-3">Largura 15%</div>
<div class="nv-w-20 nv-p-3">Largura 20%</div>
<div class="nv-w-25 nv-p-3">Largura 25%</div>
<div class="nv-w-30 nv-p-3">Largura 30%</div>
<div class="nv-w-35 nv-p-3">Largura 35%</div>
<div class="nv-w-40 nv-p-3">Largura 40%</div>
<div class="nv-w-45 nv-p-3">Largura 45%</div>
<div class="nv-w-50 nv-p-3">Largura 50%</div>
<div class="nv-w-55 nv-p-3">Largura 55%</div>
<div class="nv-w-60 nv-p-3">Largura 60%</div>
<div class="nv-w-65 nv-p-3">Largura 65%</div>
<div class="nv-w-70 nv-p-3">Largura 70%</div>
<div class="nv-w-75 nv-p-3">Largura 75%</div>
<div class="nv-w-80 nv-p-3">Largura 80%</div>
<div class="nv-w-85 nv-p-3">Largura 85%</div>
<div class="nv-w-90 nv-p-3">Largura 90%</div>
<div class="nv-w-95 nv-p-3">Largura 95%</div>
<div class="nv-w-100 nv-p-3">Largura 100%</div>
<div class="nv-w-auto nv-p-3">Largura automática</div>

<div class="nv-h-5 nv-d-inline-block">Altura 5%</div>
<div class="nv-h-10 nv-d-inline-block">Altura 10%</div>
<div class="nv-h-15 nv-d-inline-block">Altura 15%</div>
<div class="nv-h-20 nv-d-inline-block">Altura 20%</div>
<div class="nv-h-25 nv-d-inline-block">Altura 25%</div>
<div class="nv-h-30 nv-d-inline-block">Altura 30%</div>
<div class="nv-h-35 nv-d-inline-block">Altura 35%</div>
<div class="nv-h-40 nv-d-inline-block">Altura 40%</div>
<div class="nv-h-45 nv-d-inline-block">Altura 45%</div>
<div class="nv-h-50 nv-d-inline-block">Altura 50%</div>
```

Caso seja necessário, você também pode usar as classes modificadoras `.nv-mw-100` e `.nv-mh-100` para definir a largura como `max-width: 100%` e `max-height: 100%` respectivamente.

```html
<div class="nv-mw-100">Largura máxima 100%</div>

<div class="nv-mh-100" style="height: 300px">Altura máxima 100%</div>
```

## Relativo ao viewport

Você também pode definir a largura e altura em relação a viewport.  

```html
<div class="nv-min-vw-100">Largura mínima 100vw</div>
<div class="nv-min-vh-100">Altura mínima 100vh</div>
<div class="nv-vw-100">Largura 100vw</div>
<div class="nv-vh-100">Altura 100vh</div>
```
 
