---
description: Adicione sombras a elementos HTML com nossas classes modificadoras.
title: Sombras
nnp.date: 10/03/2019
nnp.topic: article
keywords: sombras, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

Sombras
Adicione sombras a elementos HTML com nossas classes modificadoras.

O Nuiverse UI está em constante evolução e possuímos um sistema de sombras que ainda está sendo desenvolvido, ele foi criado para promover hierarquia visual, quando usado corretamente as sombras indicam que elementos estão sobrepostos além de criar a sensação de profundidade. Alguns elementos nativos do Nuiverse possuem sombras ativadas por padrão, e você pode adicionar sombra a outros elementos.  

Para adicionar sombras aos elementos você precisa adicionar a classe `.nv-depth-valor`, onde valor poderá ser um dos seguintes valores, 0, 1, 2, 4, 6, 8, 10, 12, 16, 26, 32, 48, 64, 76 e 88.  

Cada um desses valores representa um nível de “profundidade” e o encorajamos fortemente a seguir a tabela abaixo, a fim de adicionar a profundidade correta ao seu elemento HTML.  

| Componentes	                | Profundidade |
| -----------                 | ------------ |
| Página	                    | 0            |
| Switches	                  | 1            |
| Barras de progresso	        | 0            |
| Mensagens	                  | 4            |
| Barras de navegação e tabs	| 8            |
| Botões	                    | 8            |
| Menu/Dropdown	              | 12           |
| Ifframe/Webview	            | 16           |
| Dialogs/Modals	            | 26           |
| Window (IceWolfOS)	        | 32           |
| Toast Notification	        | 48           |

```html
<div class="nv-depth-0"><span>0</span></div>
<div class="nv-depth-1"><span>1</span></div>
<div class="nv-depth-2"><span>2</span></div>
<div class="nv-depth-4"><span>4</span></div>
<div class="nv-depth-6"><span>6</span></div>
<div class="nv-depth-8"><span>8</span></div>

<div class="nv-depth-10"><span>10</span></div>
<div class="nv-depth-12"><span>12</span></div>
<div class="nv-depth-16"><span>16</span></div>
<div class="nv-depth-26"><span>26</span></div>

<div class="nv-depth-32"><span>32</span></div>

<div class="nv-depth-48"><span>48</span></div>
<div class="nv-depth-64"><span>64</span></div>
<div class="nv-depth-76"><span>76</span></div>
<div class="nv-depth-88"><span>88</span></div>
```
