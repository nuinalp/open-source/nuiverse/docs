---
description: Classes modificadoras para controle de alinhamento, espessura, quebras de linhas e muito mais.
title: Texto
nnp.date: 10/03/2019
nnp.topic: article
keywords: texto, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Texto
Classes modificadoras para controle de alinhamento, espessura, quebras de linhas e muito mais.  

## Alinhamento de texto
Você pode alinhar rapidamente textos adicionando nossa classe modificadora `.nv-txt-{valor}`.  

Onde valor poderá ser um dos seguintes:  

- left
- right
- center

```html
<p class="nv-txt-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ac commodo nibh, vitae volutpat felis.</p>
        
<p class="nv-txt-right">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ac commodo nibh, vitae volutpat felis.</p>

<p class="nv-txt-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ac commodo nibh, vitae volutpat felis.</p>

<p class="nv-txt-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ac commodo nibh, vitae volutpat felis.</p>
```

Também estão disponíveis variações responsivas para as classes de alinhamento.  

- .nv-txt-left
- .nv-txt-right
- .nv-txt-center
- .nv-txt-xsm-left
- .nv-txt-xsm-right
- .nv-txt-xsm-center
- .nv-txt-sm-left
- .nv-txt-sm-right
- .nv-txt-sm-center
- .nv-txt-md-left
- .nv-txt-md-right
- .nv-txt-md-center
- .nv-txt-lg-left
- .nv-txt-lg-right
- .nv-txt-lg-center
- .nv-txt-xl-left
- .nv-txt-xl-right
- .nv-txt-xl-center
- .nv-txt-xxl-left
- .nv-txt-xxl-right
- .nv-txt-xxl-center
- .nv-txt-xxxl-left
- .nv-txt-xxxl-right
- .nv-txt-xxxl-center

## Quebra de texto e Overflow

Para evitar que um texto quebre adicione a classe .nv-txt-nowrap.  

```html
<p class="nv-txt-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ac commodo nibh, vitae volutpat felis.</p>
```
Para conteúdos longos demais, você pode usar a classe `.nv-txt-truncate` para reduzir o texto com reticências. Requer o uso de `display: inline-block` ou `display: block`.  

```html
<p style="width: 200px;" class="nv-txt-truncate">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ac commodo nibh, vitae volutpat felis.</p>
```

## Transformação de texto

Para aplicar transformações em um texto, você pode utilizar uma das nossas classes de transformações, como no exemplo abaixo:  

```html
<p class="nv-txt-lowercase">texto em minúsculo.</p>
<p class="nv-txt-uppercase">texto em maiúsculas.</p>
<p class="nv-txt-capitalize">texto em maiúsculas</p>
```

## Espessura e Itálico

O Nuiverse usa por padrão a fonte FiraGO e utilizamos várias variações e espessuras dessa fonte, com isso em mente oferecemos diferentes espessuras e variações da fonte FiraGO.  

Veja a seguir as variações que você pode utilizar em seus projetos.  

```html
<p class="nv-font-regular">Texto com espessura normal.</p>
<p class="nv-font-bold">Texto com espessura em negrito.</p>
<p class="nv-font-bold-italic">texto em itálico com espessura em negrito.</p>
<p class="nv-font-italic">Texto em itálico.</p>
<p class="nv-font-medium-italic">Texto em itálico com espessura média.</p>
<p class="nv-font-light">Texto com espessura menor.</p>
<p class="nv-font-light-italic">Texto em itálico.</p>
```

Lembre-se sempre de seguir nossa tabela de variações, presente em tipografia, a fim de aplicar a variação certa.  

## Monospace

Para alterar nossa fonte principal para uma fonte monospace adicione nossa classe .nv-txt-monospace.
O Nuiverse utiliza por padrão a fonte Fira Code, para textos com fontes mono espaçadas, caso não esteja disponível uma fonte alternativa será escolhida pelo navegador.  

```html
<p class="nv-font-monospace">Monospace</p>
```
 
