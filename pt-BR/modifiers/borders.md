---
description: Use as classes modificaras de borda para controlar as bordas de um elemento.
title: Bordas
nnp.date: 10/03/2019
nnp.topic: article
keywords: bordas, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Bordas

Use as classes modificaras de borda para controlar as bordas de um elemento. Sempre que possível, use-as em vez de definir código CSS personalizado.  

## Adicionar

```html
<div class="nv-border"></div>
<div class="nv-border-top"></div>
<div class="nv-border-right"></div>
<div class="nv-border-bottom"></div>
<div class="nv-border-left"></div>
```

## Remover

```html
<div class="nv-border-0"></div>
<div class="nv-border-top-0"></div>
<div class="nv-border-right-0"></div>
<div class="nv-border-bottom-0"></div>
<div class="nv-border-left-0"></div>
```

## Cor da borda

Altere as cores das bordas usando as classes de temas feitas do sistema de cores.  
```html
<div class="nv-border nv-border-primary"></div>
<div class="nv-border nv-border-secondary"></div>
<div class="nv-border nv-border-danger"></div>
<div class="nv-border nv-border-warning"></div>
<div class="nv-border nv-border-success"></div>
```

## Radio da borda

Use as classes de bordas para arredondar facilmente as bordas de uma imagem ou elemento.

```html
<img class="nv-border-rounded" src="..." alt="...">
<img class="nv-border-rounded-top" src="..." alt="...">
<img class="nv-border-rounded-right" src="..." alt="...">
<img class="nv-border-rounded-bottom" src="..." alt="...">
<img class="nv-border-rounded-left" src="..." alt="...">
```