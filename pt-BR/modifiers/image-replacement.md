---
description: Troque textos por imagens, com nossa classe modificadora para esconder textos.
title: Substituição por imagens
nnp.date: 10/03/2019
nnp.topic: article
keywords: substituição por imagens, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---


# Substituição por imagens
Troque textos por imagens, com nossa classe modificadora para esconder textos.

Utilize a classe `.nv-text-hide` ou o mixin para ajudar a substituir o texto de um elemento por uma imagem via `background-image`.  

```html
<h1 class="nv-txt-hide">Cabeçalho personalizado</h1>
```

```scss
// Exemplo de uso do mixin
.texto {
  @include text-hide;
}
```
A classe `.nv-text-hide` é usada para esconder o texto e manter os benefícios de acessibilidade e SEO das tags de cabeçalho, enquanto utilizamos uma imagem de fundo.
 