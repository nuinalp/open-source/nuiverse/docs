---
description: Altere de maneira simples e rápida o valor da propriedade display, com nossas classes modificadoras.
title: Display
nnp.date: 10/03/2019
nnp.topic: article
keywords: display, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Display

Altere de maneira simples e rápida o valor da propriedade display, com nossas classes modificadoras, para utilizá-la basta adicionar a classe `.nv-display-{valor}` ao elemento html.  

Onde valor pode ser um dos seguintes:  

- none
- inline
- inline-block
- block
- table
- table-cell
- table-row
- flex
- inline-flex

## Exemplos

```html
<div class="square-example nv-bg-primary nv-d-inline">.nv-d-inline</div>
<div class="square-example nv-bg-danger nv-d-inline">.nv-d-inline</div>

<div class="square-example nv-bg-primary nv-d-block">.nv-d-block</div>
<div class="square-example nv-bg-danger nv-d-block">.nv-d-block</div>
```

## Ocultando elementos

Para um desenvolvimento mais rápido em dispositivos móveis utilize as classes responsivas para exibir e ocultar elementos de acordo com o tamanho da tela. Sempre que possível utilize-as e evite criar CSS customizado, isso garante a padronização e uma melhor manutenção do sistema.  

Para ocultar elementos na tela utilize a classe .nv-d-none ou uma das classes `.nv-d-{mobile, tablet, desktop, widescreen, fullhd}-none` para ocultar de acordo com o tamanho de cada tela.  

Para mostrar um elemento em determinado tamanho da tela, você pode combinar a classe `.nv-d-none` com uma classe `.nv-d-*`, por exemplo `.nv-d-none .nv-d-tablet-block`, isso fará com que o elemento seja oculto em todos os tamanhos de tela, exceto em médios e grandes.  

Veja a baixo mais combinações que podem ser utilizadas para ocultar e exibir elementos em tamanhos de telas diferentes.  

|                                                       |                               |
| ----------------------------------------------------- | ----------------------------- |
| .nv-d-none	                                          | Oculto em todos os tamanhos   |
| .nv-d-none .nv-d-tablet-block	                        | Oculto somente no mobile      |
| .nv-d-tablet-none .nv-d-desktop-block	                | Oculto somente em tablet      |
| .nv-d-desktop-none .nv-d-widescreen-block	            | Oculto somente em desktop     |
| .nv-d-widescreen-none .nv-d-fullhd-block	            | Oculto somente em widescreen  |
| .nv-d-fullhd-none	                                    | Oculto somente em fullhd      |
| .nv-d-block	                                          | Visível em todos os tamanhos  |
| .nv-d-block .nv-d-tablet-none                         |	Visível somente em mobile     |
| .nv-d-none .nv-d-tablet-block .nv-d-desktop-none	    | Visível somente em tablet     |
| .nv-d-none .nv-d-desktop-block .nv-d-widescreen-none	| Visível somente em desktop    |
| .nv-d-none .nv-d-widescreen-block .nv-d-fullhd-none	  | Visível somente em widescreen |
| .nv-d-none .nv-d-fullhd-block	                        | Visível somente em fullhd     |

## Exemplo

```html
<div class="nv-d-none">Oculto em todos os tamanhos</div>
<div class="nv-d-none nv-d-tablet-block">Oculto somente no mobile</div>
<div class="nv-d-tablet-none nv-d-desktop-block">Oculto somente em tablet</div>
<div class="nv-d-desktop-none nv-d-widescreen-block">Oculto somente em desktop</div>
<div class="nv-d-widescreen-none nv-d-fullhd-block">Oculto somente em widescreen</div>
<div class="nv-d-fullhd-none">Oculto somente em fullhd</div>
<div class="nv-d-block">Visível em todos os tamanhos</div>
<div class="nv-d-block nv-d-tablet-none">Visível somente em mobile</div>
<div class="nv-d-none nv-d-tablet-block nv-d-desktop-none">Visível somente em tablet</div>
<div class="nv-d-none nv-d-desktop-block nv-d-widescreen-none">Visível somente em desktop</div>
<div class="nv-d-none nv-d-widescreen-block nv-d-fullhd-none">Visível somente em widescreen</div>
<div class="nv-d-none nv-d-fullhd-block">Visível somente em fullhd</div>
```