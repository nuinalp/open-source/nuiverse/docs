---
description: Adicione ou remova margens e paddings em elementos HTML de maneira fácil com nossas classes modificadoras.
title: Espaçamento
nnp.date: 10/03/2019
nnp.topic: article
keywords: espaçamento, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---
# Espaçamento
Adicione ou remova margens e paddings em elementos HTML de maneira fácil com nossas classes modificadoras.  

## Como funciona

Nós fornecemos classes modificadoras para padding e margins, suportamos todas as propriedades individuais, verticais e horizontais. Essas classes são construídas a partir de um mapa scss, disponível em `src\core\src\style\utils\variables\_spaces.scss`, elas seguem nossa grade de 8 pontos e iniciam em 8px e variam até 64px. Atualmente existem 8 tamanhos e você pode personalizar nosso mapa scss a fim de obter incrementos maiores, porém recomendamos fortemente que respeite a grade 8 pontos.  

## Notação

Nossas classes se aplicam a todos os tamanhos de breakpoints que suportamos, sendo eles: **mobile**, **tablet**, **desktop**, **widescreen** e **fullhd**, você pode mistura-los a fim de obter melhores resultados.  

As classes são definidas utilizando o seguinte formato `{propriedade}{lados}-{tamanho}` e para variações responsivas `{propriedade}{lados}-{breakpoint}-{tamanho}`.  

O campo propriedade por ser:  

- p – para usar padding.
- m – para usar margin.

O campo lados pode ser:  

- t - para usar margin-top ou padding-top;
- b - para usar margin-bottom ou padding-bottom;
- l - para usar margin-left ou padding-left;
- r - para usar margin-right ou padding-right;
- x - para usar margin ou padding, tanto na esquerda quanto direita;
- y - para usar margin ou padding, tanto na parte superior quanto na inferior;

Deixe o campo em branco, para usar margin ou padding em todos os quatro lados do elemento.  
O campo tamanho pode ser:  

- 0 - para remover margin ou padding;
- 1 - para usar margin ou padding com o valor de $space-base * 1;
- 2 - para usar margin ou padding com o valor de $space-base * 2;
- 3 - para usar margin ou padding com o valor de $space-base * 3;
- 4 - para usar margin ou padding com o valor de $space-base * 4;
- 5 - para usar margin ou padding com o valor de $space-base * 5;
- 6 - para usar margin ou padding com o valor de $space-base * 6;
- 7 - para usar margin ou padding com o valor de $space-base * 7;
- 8 - para usar margin ou padding com o valor de $space-base * 8;
- auto - para usar margin com o valor de auto;

## Exemplo

Veja abaixo um exemplo de como nossas classes funcionam:  

```scss
.nv-mt-0 {
  margin-top: 0 !important;
}

.nv-ml-1 {
  margin-left: ($space-base * 1) !important;
}

.nv-px-2 {
  padding-left: ($space-base * 2) !important;
  padding-right: ($space-base * 2) !important;
}

.nv-p-3 {
  padding: $space-base * 3 !important;
}
```
 
## Centralização horizontal

Além das classes introduzidas acima, nós também oferecemos a classe .nv-mx-auto, ela centraliza elementos que possuem display:block, definindo as margens horizontais como auto.  

```html
<div class="nv-mx-auto" style="width: 200px;">
  Elementro centralizado.
</div>
```
