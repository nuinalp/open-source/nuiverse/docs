---
description: Controle a visibilidade de elementos HTML de maneira simples e rápida, sem modificar a propriedade display.
title: Visibilidade
nnp.date: 10/03/2019
nnp.topic: article
keywords: visibilidade, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Visibilidade

Controle a visibilidade de elementos HTML de maneira simples e rápida, sem modificar a propriedade display.  

Para alterar a visibilidade de elementos HTML, adicione a classe `.nv-visible` ou `.nv-invisible` para exibir ou ocultar elementos respectivamente. Nossas classes de visibilidade ocultam elementos para a maioria dos usuários, exceto para leitores de tela, visto que alteramos a propriedade visibility e não a propriedade display a fim de evitar impactar o layout.

```html
<div class="nv-visible">visible</div>
<div class="nv-invisible">invisible</div>
```
