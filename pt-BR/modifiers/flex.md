---
description: Altere o layout de forma rápida e fácil com as classes modificadoras.
title: Flex
nnp.date: 10/03/2019
nnp.topic: article
keywords: flex, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Flex
Altere o layout de forma rápida e fácil com as classes modificadoras.  

## Ativar Flexbox

Para ativar o flexbox use a classe modificadora `.nv-d-flex` em um elemento pai, para transformar seus elementos filhos em flex items. Você também pode usar outras classes complementares para alterar o comportamento dos elementos filhos.  

```html
<div class="nv-d-flex nv-p-2">Eu sou um container flexbox xD!</div>

<div class="nv-d-inline-flex nv-p-2">Eu sou um container flexbox inline xD!</div>
```

Você também pode usar variações responsivas para flex e flex-inline.  

- .nv-d-flex
- .nv-d-inline-flex
- .nv-d-xsm-flex
- .nv-d-xsm-inline-flex
- .nv-d-sm-flex
- .nv-d-sm-inline-flex
- .nv-d-md-flex
- .nv-d-md-inline-flex
- .nv-d-lg-flex
- .nv-d-lg-inline-flex
- .nv-d-xl-flex
- .nv-d-xl-inline-flex
- .nv-d-xxl-flex
- .nv-d-xxl-inline-flex
- .nv-d-xxxl-flex
- .nv-d-xxxl-inline-flex

## Direção

Altere a direção dos elementos flex com a classe modificadora. Na maioria dos casos você não precisa especificar essa classe, visto que o padrão dos navegadores é row. Entretanto podem surgir situações em que seja necessário alterar a direção, como em layouts responsivos, diante disso encorajamos fortemente o uso das classes modificaras.  

Use a classe `.nv-flex-row` para definir a direção como horizontal (padrão dos navegadores) ou `.nv-flex-reverse` para iniciar a direção no lado oposto.

```html
<div class="nv-d-flex nv-flex-row nv-mb-3">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
<div class="nv-d-flex nv-flex-row-reverse">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
```

Para definir a direção como vertical, use a classe modificadora `.nv-flex-column` ou `.nv-flex-reverse-column` para iniciar no lado oposto.  

```html
<div class="nv-d-flex nv-flex-column nv-mb-3">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
<div class="nv-d-flex nv-flex-column-reverse">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
```

Assim como em outras classes, a `.nv-flex-direction` possui suporte para variações responsivas.  

- .nv-flex-row
- .nv-flex-row-reverse
- .nv-flex-column
- .nv-flex-column-reverse
- .nv-flex-xsm-row
- .nv-flex-xsm-row-reverse
- .nv-flex-xsm-column
- .nv-flex-xsm-column-reverse
- .nv-flex-sm-row
- .nv-flex-sm-row-reverse
- .nv-flex-sm-column
- .nv-flex-sm-column-reverse
- .nv-flex-md-row
- .nv-flex-md-row-reverse
- .nv-flex-md-column
- .nv-flex-md-column-reverse
- .nv-flex-lg-row
- .nv-flex-lg-row-reverse
- .nv-flex-lg-column
- .nv-flex-lg-column-reverse
- .nv-flex-xl-row
- .nv-flex-xl-row-reverse
- .nv-flex-xl-column
- .nv-flex-xl-column-reverse
- .nv-flex-xxl-row
- .nv-flex-xxl-row-reverse
- .nv-flex-xxl-column
- .nv-flex-xxl-column-reverse
- .nv-flex-xxxl-row
- .nv-flex-xxxl-row-reverse
- .nv-flex-xxxl-column
- .nv-flex-xxxl-column-reverse

## Justificar conteúdo

Use a classe modificadora `.nv-justify-content-{valor}`, para alterar o alinhamento dos itens flex em seu eixo principal, sendo o x para alinhamento horizontal, e y no eixo vertical, caso `flex-direction: column` esteja ativado.
As variações podem ser `start` (padrão do navegador), `end`, `center`, `between`, ou `around`.  

```html
<div class="nv-d-flex nv-justify-content-start nv-mb-3">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
<div class="nv-d-flex nv-justify-content-end nv-mb-3">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
<div class="nv-d-flex nv-justify-content-center nv-mb-3">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
<div class="nv-d-flex nv-justify-content-between nv-mb-3">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
<div class="nv-d-flex nv-justify-content-around nv-mb-3">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
```  

A classe `.nv-justify-content` também possuem variações responsivas, veja a lista abaixo.  

- .nv-justify-content-start
- .nv-justify-content-end
- .nv-justify-content-center
- .nv-justify-content-between
- .nv-justify-content-around
- .nv-justify-content-xsm-start
- .nv-justify-content-xsm-end
- .nv-justify-content-xsm-center
- .nv-justify-content-xsm-between
- .nv-justify-content-xsm-around
- .nv-justify-content-sm-start
- .nv-justify-content-sm-end
- .nv-justify-content-sm-center
- .nv-justify-content-sm-between
- .nv-justify-content-sm-around
- .nv-justify-content-md-start
- .nv-justify-content-md-end
- .nv-justify-content-md-center
- .nv-justify-content-md-between
- .nv-justify-content-md-around
- .nv-justify-content-lg-start
- .nv-justify-content-lg-end
- .nv-justify-content-lg-center
- .nv-justify-content-lg-between
- .nv-justify-content-lg-around
- .nv-justify-content-xl-start
- .nv-justify-content-xl-end
- .nv-justify-content-xl-center
- .nv-justify-content-xl-between
- .nv-justify-content-xl-around
- .nv-justify-content-xxl-start
- .nv-justify-content-xxl-end
- .nv-justify-content-xxl-center
- .nv-justify-content-xxl-between
- .nv-justify-content-xxl-around
- .nv-justify-content-xxxl-start
- .nv-justify-content-xxxl-end
- .nv-justify-content-xxxl-center
- .nv-justify-content-xxxl-between
- .nv-justify-content-xxxl-around

## Alinhar itens

Você pode alterar o alinhamento dos itens de um container flexbox usando nossa classe modificadora `.nv-align-items`, no eixo transversal, o eixo x será iniciado por padrão e o eixo y caso flex-direction: column esteja ativado.  

A classe `.nv-align-items` também possui variações responsivas, veja a lista abaixo.  

- .nv-align-items-start
- .nv-align-items-end
- .nv-align-items-center
- .nv-align-items-baseline
- .nv-align-items-stretch
- .nv-align-items-xsm-start
- .nv-align-items-xsm-end
- .nv-align-items-xsm-center
- .nv-align-items-xsm-baseline
- .nv-align-items-xsm-stretch
- .nv-align-items-sm-start
- .nv-align-items-sm-end
- .nv-align-items-sm-center
- .nv-align-items-sm-baseline
- .nv-align-items-sm-stretch
- .nv-align-items-sm-start
- .nv-align-items-sm-end
- .nv-align-items-sm-center
- .nv-align-items-sm-baseline
- .nv-align-items-sm-stretch
- .nv-align-items-md-start
- .nv-align-items-md-end
- .nv-align-items-md-center
- .nv-align-items-md-baseline
- .nv-align-items-md-stretch
- .nv-align-items-lg-start
- .nv-align-items-lg-end
- .nv-align-items-lg-center
- .nv-align-items-lg-baseline
- .nv-align-items-lg-stretch
- .nv-align-items-xl-start
- .nv-align-items-xl-end
- .nv-align-items-xl-center
- .nv-align-items-xl-baseline
- .nv-align-items-xl-stretch
- .nv-align-items-xxl-start
- .nv-align-items-xxl-end
- .nv-align-items-xxl-center
- .nv-align-items-xxl-baseline
- .nv-align-items-xxl-stretch
- .nv-align-items-xxxl-start
- .nv-align-items-xxxl-end
- .nv-align-items-xxxl-center
- .nv-align-items-xxxl-baseline
- .nv-align-items-xxxl-stretch

## Auto alinhamento

Use a classe modificadora `.nv-align-self-{valor} `para alinhar um elemento descendente de um elemento pai com a propriedade `display: flex`, no eixo x, padrão dos navegadores ou no eixo Y se `flex-direction: column` estiver ativo.

Essa classe irá ignorar as declarações de alinhamento do elemento pai, alinhando somente o próprio elemento. Veja o exemplo abaixo:

```html
<div class="nv-d-flex nv-align-items-start nv-mb-3" style="height:100px;">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
<div class="nv-d-flex nv-align-items-end nv-mb-3" style="height:100px;">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
<div class="nv-d-flex nv-align-items-center nv-mb-3" style="height:100px;">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
<div class="nv-d-flex nv-align-items-baseline nv-mb-3" style="height:100px;">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
<div class="nv-d-flex nv-align-items-stretch nv-mb-3" style="height:100px;">
  <div class="nv-p-2">Flex item 1</div>
  <div class="nv-p-2">Flex item 2</div>
  <div class="nv-p-2">Flex item 3</div>
</div>
```

Você também pode usar as variações responsivas, conforme a lista abaixo:  

- .nv-align-self-start
- .nv-align-self-end
- .nv-align-self-center
- .nv-align-self-baseline
- .nv-align-self-stretch
- .nv-align-self-xsm-start
- .nv-align-self-xsm-end
- .nv-align-self-xsm-center
- .nv-align-self-xsm-baseline
- .nv-align-self-xsm-stretch
- .nv-align-self-sm-start
- .nv-align-self-sm-end
- .nv-align-self-sm-center
- .nv-align-self-sm-baseline
- .nv-align-self-sm-stretch
- .nv-align-self-md-start
- .nv-align-self-md-end
- .nv-align-self-md-center
- .nv-align-self-md-baseline
- .nv-align-self-md-stretch
- .nv-align-self-lg-start
- .nv-align-self-lg-end
- .nv-align-self-lg-center
- .nv-align-self-lg-baseline
- .nv-align-self-lg-stretch
- .nv-align-self-xl-start
- .nv-align-self-xl-end
- .nv-align-self-xl-center
- .nv-align-self-xl-baseline
- .nv-align-self-xl-stretch
- .nv-align-self-xxl-start
- .nv-align-self-xxl-end
- .nv-align-self-xxl-center
- .nv-align-self-xxl-baseline
- .nv-align-self-xxl-stretch
- .nv-align-self-xxxl-start
- .nv-align-self-xxxl-end
- .nv-align-self-xxxl-center
- .nv-align-self-xxxl-baseline
- .nv-align-self-xxxl-stretch

## Preenchimento

Use a classe modificadora `.nv-flex-fill` em vários elementos para força-los a terem larguras idênticas, usando para isso todo o espaço horizontal disponível, veja o exemplo abaixo:  

```html
<div class="nv-d-flex nv-mb-3">
  <div class="nv-p-2 nv-flex-fill">Flex item</div>
  <div class="nv-p-2 nv-flex-fill">Flex item</div>
  <div class="nv-p-2 nv-flex-fill">Flex item</div>
</div>
```

As variações responsivas também estão disponíveis para a classe `.nv-flex-fill`, veja a lista a seguir:  

- .nv-flex-fill
- .nv-flex-xsm-fill
- .nv-flex-sm-fill
- .nv-flex-md-fill
- .nv-flex-lg-fill
- .nv-flex-xl-fill
- .nv-flex-xxl-fill
- .nv-flex-xxxl-fill

## Grow e shrink
Use a classe modificadora `.nv-flex-grow` para fazer com que um elemento use todo os espaço disponível. No exemplo abaixo, o elemento `.nv-flex-grow-1` usa todo o espaço disponível, enquanto os itens restantes usam somente o necessário.  

```html
<div class="nv-d-flex nv-mb-3">
  <div class="nv-p-2 nv-flex-grow-1">Flex item with a lot of content</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>
```

Use a classe modificadora `.nv-flex-shrink-*` para fazer com que o elemento seja forçado a quebrar linha e oferecer mais espaço para o elemento que possui `.nv-w-100`.  

```html
<div class="nv-d-flex nv-mb-3">
  <div class="nv-p-2 nv-w-100">Flex item with a lot of content</div>
  <div class="nv-p-2 nv-flex-shrink-1">Flex item</div>
</div>
```

As variações responsivas também estão disponíveis para as classes `.nv-flex-shrink-*` e `.nv-flex-grow`.

- .nv-flex-{grow|shrink}-0
- .nv-flex-{grow|shrink}-1
- .nv-flex-xsm-{grow|shrink}-0
- .nv-flex-xsm-{grow|shrink}-1
- .nv-flex-sm-{grow|shrink}-0
- .nv-flex-sm-{grow|shrink}-1
- .nv-flex-md-{grow|shrink}-0
- .nv-flex-md-{grow|shrink}-1
- .nv-flex-lg-{grow|shrink}-0
- .nv-flex-lg-{grow|shrink}-1
- .nv-flex-xl-{grow|shrink}-0
- .nv-flex-xl-{grow|shrink}-1
- .nv-flex-xxl-{grow|shrink}-0
- .nv-flex-xxl-{grow|shrink}-1
- .nv-flex-xxxl-{grow|shrink}-0
- .nv-flex-xxxl-{grow|shrink}-1

## Margens automáticas

Você pode obter resultados impressionantes ao misturar os diferentes recursos oferecidos pelo navegador, ao misturar flexbox (`.nv-d-flex`) e margens (`.nv-m{valor}-auto` ou `.nv-m{valor}-auto`) é possível obter maior controle e personalização do layout. Abaixo são fornecidos 3 exemplos de como controlar itens flex usando margens automáticas: padrão (sem margens automáticas), empurrando dois itens à direita (`.nv-mr-auto`) e empurrando dois itens a esquerda (`.nv-ml-auto`).

```html
<div class="nv-d-flex nv-mb-3">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>

<div class="nv-d-flex nv-mb-3">
  <div class="nv-mr-auto p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>

<div class="nv-d-flex nv-mb-3">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-ml-auto p-2">Flex item</div>
</div>
```

## Align-items

Mova um item flex para a parte superior ou inferior de um container HTML, combinando nossa classe .nv-align-items com nossas classes modificadoras para margens `.nv-mt-auto` ou `.nv-mb-auto`.  

```html
<div class="nv-d-flex nv-align-items-start nv-flex-column nv-mb-3" style="height: 200px;">
  <div class="nv-mb-auto nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>

<div class="nv-d-flex nv-align-items-end nv-flex-column nv-mb-3" style="height: 200px;">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-mt-auto nv-p-2">Flex item</div>
</div>
```

## Quebra

Altere como os itens flex são quebrados dentro de um elemento HTML com `display-flex` (`.nv-d-flex`), você pode escolher entre nunca quebrar (padrão dos navegadores), quebrar e quebra-reversa, usando para isso nossas classes modificadoras `.nv-flex-nowrap`, `.nv-flex-wrap` ou `.nv-flex-wrap-reverse`, respectivamente.

```html
<div class="nv-d-flex nv-flex-nowrap" style="width: 100px;">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>

<div class="nv-d-flex nv-flex-wrap">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>

<div class="nv-d-flex nv-flex-wrap-reverse">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>
```

As classes de quebra também possuem variações responsiva, conforme a lista abaixo:  

- .nv-flex-nowrap
- .nv-flex-wrap
- .nv-flex-wrap-reverse
- .nv-flex-xsm-nowrap
- .nv-flex-xsm-wrap
- .nv-flex-xsm-wrap-reverse
- .nv-flex-sm-nowrap
- .nv-flex-sm-wrap
- .nv-flex-sm-wrap-reverse
- .nv-flex-md-nowrap
- .nv-flex-md-wrap
- .nv-flex-md-wrap-reverse
- .nv-flex-lg-nowrap
- .nv-flex-lg-wrap
- .nv-flex-lg-wrap-reverse
- .nv-flex-xl-nowrap
- .nv-flex-xl-wrap
- .nv-flex-xl-wrap-reverse
- .nv-flex-xxl-nowrap
- .nv-flex-xxl-wrap
- .nv-flex-xxl-wrap-reverse
- .nv-flex-xxxl-nowrap
- .nv-flex-xxxl-wrap
- .nv-flex-xxxl-wrap-reverse

## Ordem

Em alguns casos pode ser muito útil ou necessário alterar a ordem visual dos elementos apresentados na tela, diante disso, nós fornecemos classes modificadoras que facilitam esse trabalho, para conseguir esse efeito basta adicionar a classe `.nv-order-{valor}`, onde valor é um número inteiro de 0 à 12.

Você pode usar CSS personalizado para criar valores adicionais se necessário.  

```html
<div class="nv-d-flex nv-flex-wrap-reverse">
  <div class="nv-p-2 nv-order-3">1</div>
  <div class="nv-p-2 nv-order-1">2</div>
  <div class="nv-p-2 nv-order-2">3</div>
</div>
```

Nós também oferecemos variações responsivas para .nv-order-{valor}.  

- .nv-order-0
- .nv-order-1
- .nv-order-2
- .nv-order-3
- .nv-order-4
- .nv-order-5
- .nv-order-6
- .nv-order-7
- .nv-order-8
- .nv-order-9
- .nv-order-10
- .nv-order-11
- .nv-order-12
- .nv-order-xsm-0
- .nv-order-xsm-1
- .nv-order-xsm-2
- .nv-order-xsm-3
- .nv-order-xsm-4
- .nv-order-xsm-5
- .nv-order-xsm-6
- .nv-order-xsm-7
- .nv-order-xsm-8
- .nv-order-xsm-9
- .nv-order-xsm-10
- .nv-order-xsm-11
- .nv-order-xsm-12
- .nv-order-sm-0
- .nv-order-sm-1
- .nv-order-sm-2
- .nv-order-sm-3
- .nv-order-sm-4
- .nv-order-sm-5
- .nv-order-sm-6
- .nv-order-sm-7
- .nv-order-sm-8
- .nv-order-sm-9
- .nv-order-sm-10
- .nv-order-sm-11
- .nv-order-sm-12
- .nv-order-md-0
- .nv-order-md-1
- .nv-order-md-2
- .nv-order-md-3
- .nv-order-md-4
- .nv-order-md-5
- .nv-order-md-6
- .nv-order-md-7
- .nv-order-md-8
- .nv-order-md-9
- .nv-order-md-10
- .nv-order-md-11
- .nv-order-md-12
- .nv-order-lg-0
- .nv-order-lg-1
- .nv-order-lg-2
- .nv-order-lg-3
- .nv-order-lg-4
- .nv-order-lg-5
- .nv-order-lg-6
- .nv-order-lg-7
- .nv-order-lg-8
- .nv-order-lg-9
- .nv-order-lg-10
- .nv-order-lg-11
- .nv-order-lg-12
- .nv-order-xl-0
- .nv-order-xl-1
- .nv-order-xl-2
- .nv-order-xl-3
- .nv-order-xl-4
- .nv-order-xl-5
- .nv-order-xl-6
- .nv-order-xl-7
- .nv-order-xl-8
- .nv-order-xl-9
- .nv-order-xl-10
- .nv-order-xl-11
- .nv-order-xl-12
- .nv-order-xxl-0
- .nv-order-xxl-1
- .nv-order-xxl-2
- .nv-order-xxl-3
- .nv-order-xxl-4
- .nv-order-xxl-5
- .nv-order-xxl-6
- .nv-order-xxl-7
- .nv-order-xxl-8
- .nv-order-xxl-9
- .nv-order-xxl-10
- .nv-order-xxl-11
- .nv-order-xxl-12
- .nv-order-xxxl-0
- .nv-order-xxxl-1
- .nv-order-xxxl-2
- .nv-order-xxxl-3
- .nv-order-xxxl-4
- .nv-order-xxxl-5
- .nv-order-xxxl-6
- .nv-order-xxxl-7
- .nv-order-xxxl-8
- .nv-order-xxxl-9
- .nv-order-xxxl-10
- .nv-order-xxxl-11
- .nv-order-xxxl-12


## Align content

Altere facilmente o alinhamento de todos os itens de um container flex, com nossa classe modificadora `.nv-align-content-{valor}`, onde valor poderá ser um dos seguintes: `start` (padrão dos browsers), `end`, `center`, `between`, `around` ou `stretch`. Veja o exemplo a seguir.  

Atenção: Essa classe modificadora não terá efeito em containers com linha única.  

```html
<div class="nv-d-flex nv-align-content-end nv-flex-wrap" style="height: 200px;">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>


<div class="nv-d-flex nv-align-content-center nv-flex-wrap" style="height: 200px;">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>

<div class="nv-d-flex nv-align-content-between nv-flex-wrap" style="height: 200px;">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>
 
<div class="nv-d-flex nv-align-content-around nv-flex-wrap" style="height: 200px;">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>

<div class="nv-d-flex nv-align-content-stretch nv-flex-wrap" style="height: 200px;">
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
  <div class="nv-p-2">Flex item</div>
</div>
```

Nós também fornecemos variações responsivas para `.nv-align-content-{valor}`.  

- .nv-align-content-start
- .nv-align-content-end
- .nv-align-content-center
- .nv-align-content-around
- .nv-align-content-stretch
- .nv-align-content-xsm-start
- .nv-align-content-xsm-end
- .nv-align-content-xsm-center
- .nv-align-content-xsm-around
- .nv-align-content-xsm-stretch
- .nv-align-content-sm-start
- .nv-align-content-sm-end
- .nv-align-content-sm-center
- .nv-align-content-sm-around
- .nv-align-content-sm-stretch
- .nv-align-content-md-start
- .nv-align-content-md-end
- .nv-align-content-md-center
- .nv-align-content-md-around
- .nv-align-content-md-stretch
- .nv-align-content-lg-start
- .nv-align-content-lg-end
- .nv-align-content-lg-center
- .nv-align-content-lg-around
- .nv-align-content-lg-stretch
- .nv-align-content-xl-start
- .nv-align-content-xl-end
- .nv-align-content-xl-center
- .nv-align-content-xl-around
- .nv-align-content-xl-stretch
- .nv-align-content-xxl-start
- .nv-align-content-xxl-end
- .nv-align-content-xxl-center
- .nv-align-content-xxl-around
- .nv-align-content-xxl-stretch
- .nv-align-content-xxxl-start
- .nv-align-content-xxxl-end
- .nv-align-content-xxxl-center
- .nv-align-content-xxxl-around
- .nv-align-content-xxxl-stretch
