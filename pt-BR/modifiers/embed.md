---
description: Incorpore vídeos, slides ou qualquer outro conteúdo em suas páginas web ou aplicações de maneira fácil.
title: Incorporações
nnp.date: 10/03/2019
nnp.topic: article
keywords: incorporações, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Incorporações

Incorpore vídeos, slides ou qualquer outro conteúdo em suas páginas web ou aplicações de maneira fácil.  

Nosso componente escala para qualquer tamanho de dispositivo e baseia-se na largura do elemento pai.  

A regra é aplicada automaticamente aos elementos `<iframe>`, `<embed>`, `<video>` e `<object>`, além da opção de web component você pode utilizar divs para incorporar seus conteúdos utilizando a classe .nv-embed-responsive-item.  

## Exemplo

```html
<div class="nv-embed-responsive nv-embed-responsive-16by9">
  <iframe src="https://www.youtube.com/embed/3PFJrUN7BXw" allowfullscreen></iframe>
</div>
```

As proporções podem ser personalizadas com a classe modificadora, nv-embed-responsive-{valor}.  

O campo valor poderá ser: 

- 16by9
- 21by9
- 4by3
- 1by1


```html
<div class="nv-embed-responsive nv-embed-responsive-16by9">
  <iframe src="https://www.youtube.com/embed/3PFJrUN7BXw" allowfullscreen></iframe>
</div>
  
<div class="nv-embed-responsive nv-embed-responsive-21by9">
  <iframe src="https://www.youtube.com/embed/3PFJrUN7BXw" allowfullscreen></iframe>
</div>
  
<div class="nv-embed-responsive nv-embed-responsive-4by3">
  <iframe src="https://www.youtube.com/embed/3PFJrUN7BXw" allowfullscreen></iframe>
</div>
  
<div class="nv-embed-responsive nv-embed-responsive-1by1">
  <iframe src="https://www.youtube.com/embed/3PFJrUN7BXw" allowfullscreen></iframe>
</div>
```