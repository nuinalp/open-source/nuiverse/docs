---
description: Adicione ou altere a cor de elementos do Nuiverse ou textos com as classes modificadoras, elas foram construídas com base no sistema de temas do Nuiverse.
title: Cores
nnp.date: 10/03/2019
nnp.topic: article
keywords: cores, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Cores

Adicione ou altere a cor de elementos do Nuiverse ou textos com as classes modificadoras, elas foram construídas com base no sistema de temas do Nuiverse.  

Para textos adicione a classe `.nv-txt-{nome-da-cor}`.  

```html
<p class="nv-txt-primary">.nv-txt-primary</p>
<p class="nv-txt-secondary">.nv-txt-secondary</p>
<p class="nv-txt-success">.nv-txt-success</p>
<p class="nv-txt-danger">.nv-txt-danger</p>
<p class="nv-txt-warning">.nv-txt-warning</p>
<p class="nv-txt-info">.nv-txt-info</p>
```

As classes de cores para textos também funcionam em elementos de âncora.

```html
<p><a href="#" class="nv-txt-primary">Primary link</a></p>
<p><a href="#" class="nv-txt-secondary">Secondary link</a></p>
<p><a href="#" class="nv-txt-success">Success link</a></p>
<p><a href="#" class="nv-txt-danger">Danger link</a></p>
<p><a href="#" class="nv-txt-warning">Warning link</a></p>
<p><a href="#" class="nv-txt-info">Info link</a></p>
```

## Cores de fundo

Você também pode adicionar um fundo para elementos de maneira simples, usando as classe `.nv-bg-{nome-da-cor}`.

```html
<div class="nv-p-3 nv-mb-2 nv-bg-primary nv-txt-white">.nv-bg-primary</div>
<div class="nv-p-3 nv-mb-2 nv-bg-secondary nv-txt-white">.nv-bg-secondary</div>
<div class="nv-p-3 nv-mb-2 nv-bg-success nv-txt-white">.nv-bg-success</div>
<div class="nv-p-3 nv-mb-2 nv-bg-danger nv-txt-white">.nv-bg-danger</div>
<div class="nv-p-3 nv-mb-2 nv-bg-warning nv-txt-dark">.nv-bg-warning</div>
<div class="nv-p-3 nv-mb-2 nv-bg-info nv-txt-white">.nv-bg-info</div>
<div class="nv-p-3 nv-mb-2 nv-bg-light nv-txt-dark">.nv-bg-light</div>
<div class="nv-p-3 nv-mb-2 nv-bg-transparent nv-txt-dark">.nv-bg-transparent</div> 
```