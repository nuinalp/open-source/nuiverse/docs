---
description: Limpe elementos flutuantes adicionando a classe .nv-clearfix ao elemento pai, ela também pode ser usado como mixin.
title: Clearfix
nnp.date: 10/03/2019
nnp.topic: article
keywords: clearfix, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---
```html
<div class="nv-clearfix">...</div>
```

```scss
@mixin clearfix() {
  &::after {
    clear: both;
    content: '';
    display: block;
  }
}
```

O exemplo a seguir, mostra como a classe .nv-clearfix pode ser usada. Sem ela a div mãe não se expandiria ao redor dos botões.  

```html
<div class="clearfix-example nv-clearfix">
  <nv-button type="button" class="nv-float-left">Example Button floated left</nv-button>
  <nv-button type="button" class="nv-float-right">Example Button floated right</nv-button>
</div> 
```