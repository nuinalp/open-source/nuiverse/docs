---
description: Altere de maneira simples o alinhamento vertical de elementos inline, inline-block, inline-table ou células de tabelas;
title: Alinhamento vertical
nnp.date: 10/03/2019
nnp.topic: article
keywords: alinhamento, vertical, alinhamento vertical, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Alinhamento vertical

Altere de maneira simples o alinhamento vertical de elementos inline, inline-block, inline-table ou células de tabelas.  

Para alterar o alinhamento dos elementos você deve aplicar nossa classe modificadora `.nv-align-{valor}`.  

O valor poderá ser um dos seguintes:  

- baseline
- top
- middle
- bottom
- text-top
- text-bottom

```html
<span class="nv-align-baseline">baseline</span>
<span class="nv-align-top">top</span>
<span class="nv-align-middle">middle</span>
<span class="nv-align-bottom">bottom</span>
<span class="nv-align-text-top">text-top</span>
<span class="nv-align-text-bottom">text-bottom</span>
```

Uso em células de tabelas:  

```html
<table style="height: 100px;">
  <tbody>
    <tr>
      <td class="nv-align-baseline">baseline</td>
      <td class="nv-align-top">top</td>
      <td class="nv-align-middle">middle</td>
      <td class="nv-align-bottom">bottom</td>
      <td class="nv-align-text-top">text-top</td>
      <td class="nv-align-text-bottom">text-bottom</td>
    </tr>
  </tbody>
</table>
```