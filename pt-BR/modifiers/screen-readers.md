---
description: Oculte elementos de suas páginas, menos para os leitores de tela com nossas classes modificadoras.
title: Leitores de tela
nnp.date: 10/03/2019
nnp.topic: article
keywords: leitores de tela, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Leitores de tela

Oculte elementos de suas páginas, menos para os leitores de tela com nossas classes modificadoras.  

Para ocultar um elemento HTML para todos os dispositivos, exceto para os leitores de tela adicione a classe modificadora `.nv-sr-only` ao seu elemento HTML. Você também pode combinar `.nv-sr-only` com a classe `.nv-sr-only-focusable` para fazer com que o elemento seja visível quando estiver com foco, por exemplo quando um usuário realiza navegação somente pelo teclado.  

```html
<a class="nv-sr-only nv-sr-only-focusable" href="#content">Pular conteúdo.</a>
```

Você também pode utiliza-lo como mixin SCSS:  

```scss
// Usando como Mixin.
.pular-navegacao {
  @include sr-only;
  @include sr-only-focusable;
}
```
