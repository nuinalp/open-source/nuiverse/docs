---
description: Introdução ao Nuiverse UI o sistema de design da Nuinalp.
title: Sobre o Nuiverse UI
nnp.date: 11/17/2020
nnp.topic: article
keywords: introdução, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Sobre o Nuiverse UI

Nuiverse UI é um sistema de design de código aberto da Nuinalp para produtos e experiências digitais, consiste em uma série de recursos e princípios para criarmos uma experiência fantástica e rica.

## Introdução

Como sistema de design oficial da Nuinalp, o Nuiverse UI visa atender designers e desenvolvedores que criam produtos e experiências digitais. Ele é mantido atualmente por uma pequena equipe de designers e desenvolvedores com sede no interior de São Paulo no Brasil.

Os objetivos do nosso sistema de design incluem oferecer uma comunicação compartilhada entre desenvolvedores e designers, acelerar o desenvolvimento, criar sistemas padronizados e harmônicos, além de guiarmos os desenvolvedores e designers nas melhores práticas.

## Princípios

**Open source**  
O Nuiverse UI é open source e gratuito, é desenvolvido seguindo as diretrizes do movimento de código aberto. Seus usuários também são seus criadores e todos são incentivados a contribuir.

**Inclusivo**  

O Nuiverse UI foi projetado e é desenvolvido ativamente para ser acessível por todos independente da tecnologia usada ou condições.

**Modular**  

A modularidade e flexibilidade do sistema garantem que os componentes do mesmo funcionem perfeitamente entre si em qualquer combinação que atenda suas necessidades.

**Usuário em primeiro lugar**

Nosso sistema foca no usuário e na criação de uma experiência incrível e satisfatória, todas as animações e feedbacks foram pensados nisso, focamos nas necessidades e desejos dos usuários.

**Consistência**  

O Nuiverse UI foi projetado para criar consistência garantindo assim que todos os seus componentes funcionem elegantemente juntos para garantir experiências coesas e padronizadas.

**Responsivo**  

A muito tempo planejamos nosso sistema operacional, feito inteiramente com tecnologias Web, o Nuiverse UI também visa ser utilizado em nosso sistema e por isso garantimos que seus componentes sejam acessíveis em diferentes tamanhos de tela, e em diferentes dispositivos com ou sem Touch. Em razão disso, os componentes devem responder a essas expectativas e criarem experiências satisfatórias em cada um desses cenários, seus componentes devem estar sempre prontos para responderem adequadamente.

## Governança

Nós incentivos fortemente a adoção de nosso sistema, incentivamos também a comunidade e contribuições, o sistema é mantido principalmente por uma equipe pequena juntamente com um concelho de designers e desenvolvedores.

## Adoção

**Nos envolvemos com a comunidade.** Nós nos esforçamos para manter o Nuiverse UI sempre atualizado e com novidades, também trabalhamos para que ele seja o melhor sistema de design do mundo, estamos sempre abertos a feedbacks e aceitamos contribuições de todos os tipos. Nós nos comunicamos com a comunidade por meio do Twitter e Gitlab, publicaremos roteiros e discussões na GitLab para criar transparência e imersão com nossos contribuidores e usuários.  

**Os componentes possuem uma lista de contribuintes.** A fim de criar um sistema opensource e que todos possam contribuir, os componentes do Nuiverse UI possuem uma lista de mantenedores e sempre fazemos verificações e analises para garantir que tudo esteja atualizado e funcionando, porém caso encontremos algum componente sem um mantenedor e neste caso decidirmos que não o manteremos, ele será rebaixado, ganhará uma etiqueta “Experimental” e será removido dos próximos releases, caso ele volte a ser desenvolvido novamente, será analisado e incluso novamente.

## Contribuições

Nós aceitamos contribuições de todos os tipos e com poucas barreias, para isso nós identificamos cada contribuição da seguinte forma:  

**Experimental**  

Neste estado, os componentes podem possuir padrões duplicados ou não possuírem alguns de nossos padrões, neste estágio é comum serem testados diferentes conceitos, os desenvolvedores ainda não assumiram um compromisso e estes componentes não estarão disponíveis na versão de produção.

**Beta**  

Significa que os componentes já estão em estágio de desenvolvimento mais avançado e estável, os mantenedores foram identificados e estão compromissados com o suporte do componente, existem documentações quase completas e a expectativa é de que sejam enviados para a produção com pouca ou nenhuma modificação.

**Produção**  

Os componentes estão finalizados, testados e estáveis, a documentação está completa, os mantenedores compromissados com o suporte e o componente oferece alta qualidade.

## Disponibilidade

O Nuiverse UI é construído sobre a plataforma dos Web componentes, através do _**Stencil**_, o que significa que nosso sistema de design pode ser usado com os frameworks mais famosos do mercado, incluindo mais não limitado a: Vue, React, Angular e etc....  

A primeiro momento nosso plano é oferecer suporte oficial para: Vue, React, Angular e Svelte, planos futuros envolvem suporte a outras bibliotecas como Ember, Prect entre outras. Entretanto devido ao tamanho de nossa equipe esse suporte poderá demorar a ser lançado, caso você esteja interessado em contribuir e criar as ligações necessárias para o framework que você usa, por favor consulte nosso **guia de ligações**.