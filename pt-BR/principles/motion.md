---
description: O movimento oferece feedback inteligente das interações do usuário, mantem a interface viva, guia a navegação e o conecta o usuário à interface.
title: Movimento e animações no Nuiverse UI.
nnp.date: 11/22/2020
nnp.topic: article
keywords: animações, movimento, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# _Movimento_
No Nuiverse o movimento e as animações possuem um propósito. O movimento oferece feedback inteligente das interações do usuário, mantém a interface viva, guia a navegação e o conecta o usuário à interface.  

As animações no Nuiverse UI são guiadas por 3 princípios: Objetivo, Responsivo e Divertido.  

**Objetivo**
Opte por animações simples e intencionais que promovam compreensão e clareza.  

**Divertido**
Nosso movimento procura ser divertido, cativante e agradável para nossos usuários, com esse princípio em mente nós criamos uma experiencia e uma personalidade única.  

**Responsivo**
Nossos princípios são moldados com base em uma experiencia chamativa e convidativa. O Nuiverse UI tem por objetivo levar a interação para outro nível, criando valor, convidando a interação e encantando o usuário com respostas inteligentes e chamativas. Podemos identificar diversos elementos onde podemos emitir esse conceito.  

## Tempo

As animações do Nuiverse possuem três tempos básicos, um para entrada, saída e animação de elementos visuais.  

**Entrada**
Os elementos que são apresentados na tela, possuem animações de 300ms, isso possibilita maior recepção dos elementos na tela.  

**Saída**
Elementos que saem da cena devem ter animações de 150ms.  

**Animação**
Elementos que mudam de estado, tamanho ou que possuem suas posições alteradas devem usar animações de 500ms.  

## Suavização

As animações do Nuiverse devem possuir suavização para aplicar um efeito mais realista de que os objetos estão se movendo. Com essas suavizações, tornamos as animações mais divertidas e ajudamos a reforçar a sensação física de que os elementos estão se movendo no sistema.  

**Entrada (Desacelerar)**
Usada para elementos que estão surgindo na tela.

**Saída (Acelerar)**
Usada para elementos que estão saindo da tela

**Animação (Suavização padrão)**
Base utilizada para qualquer elemento que tenham sua base alterada, seja tamanho, posição entre outros.
