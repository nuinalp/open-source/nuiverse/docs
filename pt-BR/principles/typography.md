---
description: Entenda como funciona Tipografia no Nuiverse UI e saiba como usá-la para ajudar os usuários a entenderem o contéudo facilmente.
title: Tipografia do Nuiverse UI.
nnp.date: 11/22/2020
nnp.topic: article
keywords: tipografia, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# _Tipografia_
A principal função da tipografia é informar. Seu estilo nunca deve atrapalhar sua missão, neste artigo veremos como é o sistema de tipografia do Nuiverse UI.

## Fonte padrão
No Nuiverse a fonte padrão é a [FiraGO](https://bboxtype.com/typefaces/FiraGO/#!layout=specimen), nós também utilizamos o sistema de grade de 4 pontos para nossa tipografia, e recomendamos fortemente que você a mantenha, nosso design foi projetado para combinar com a FiraGO e todas as variações que utilizamos podem não combinar com outras fontes.

## Variações de fontes:
Nós utilizamos os diferentes tipos de variações da FiraGO para diferentes caso de uso. Por exemplo utilizamos o negrito e itálico para títulos, e “Regular” para texto da página, além disso utilizamos diferentes tamanhos de fontes para criar hierarquia visual, veja abaixo nossa tabela que ilustra de maneira clara este cenário.  

Abaixo veremos de maneira mais detalhada o uso de cada tamanho e estilo de fonte:  

| Categoria da escala   | Typeface	| Variação da fonte | Tamanho |	Line height |
| -------------------   | --------- | ----------------- | ------- | ----------- |
| H1	                  | FiraGO	  | Light	            | 40	    | 44          |
| H2	                  | FiraGO	  | Light	            | 36	    | 40          |
| H3	                  | FiraGO	  | Regular	          | 32	    | 36          |
| H4	                  | FiraGO	  | Regular	          | 28	    | 32          |
| H5	                  | FiraGO	  | Regular	          | 24	    | 28          |
| H6	                  | FiraGO	  | Medium	          | 20	    | 24          |
| NavigationView Header | FiraGO	  | Bold Italic	      | 20	    | 24          |
| Body	                | FiraGO	  | Regular	          | 16	    | 20          |
| Botão	                | FiraGO	  | Medium Italic	    | 14	    | 18          |
| Logo Nuinalp	        | FiraGO	  | Semi-Bold	        | -	      | -           |
| Sub logo	            | FiraGO	  | Book            	| -	      | -           |
| Code demo	            | Fira Code	| Regular	          | 16	    | 20          |

## Alinhamento

O Nuiverse alinha os textos por padrão à esquerda, essa abordagem possui recuo à esquerda e irregular à direita, criando um design mais uniforme. Atualmente não suportamos alinhamento para idiomas da direita para a esquerda.

## Recorte e elipses

Quando um texto se estende para além do tamanho da caixa, nós recomendamos cortá-lo e aplicar o efeito “elipses”, esse é o comportamento padrão dos componentes do Nuiverse UI que trabalham com texto.

## Idiomas

A FiraGO é nossa fonte padrão para Inglês, Português, idiomas europeus, Cirílico, Grego, Vietnamita, Árabe, Tai, Georgiana, Devanágari e Hebraico.
Há planos para suportar outras línguas futuramente.

## Cabeçalhos
Todos os cabeçalhos HTML (h1 até h6) estão disponíveis.  

```html
<h1>H1</h1>
<h2>H2</h2>
<h3>H3</h3>
<h4>H4</h4>
<h5>H5</h5>
<h6>H6</h6>
```

Nós também oferecemos classes modificadoras para que você aplique o estilo de um cabeçalho.  

```html
<p class="nv-h1">.nv-h1</p>
<p class="nv-h2">.nv-h2</p>
<p class="nv-h3">.nv-h3</p>
<p class="nv-h4">.nv-h4</p>
<p class="nv-h5">.nv-h5</p>
<p class="nv-h6">.nv-h6</p>
```

## Personalizando títulos

Você pode personalizar os cabeçalhos usando nossas classes modificadoras e criar um cabeçalho secundário.

```html
<h3>Nuiverse designed by <small class="nv-txt-muted">Nuinalp</small></h3>
```

## Parágrafo destaque

Destaque um parágrafo adicionado nossa classe modificadora `.nv-lead`.

```html
<p class="nv-lead">Usando a classe .nv-lead para destacar um texto.</p>
```

## Elementos de texto inline

Oferecemos estilos para os elementos inlines mais comuns do HTML.

```html
<p>Você pode usar a tag mark para <mark>realçar</mark> textos.</p>
<p><del>Esta linha de texto deve ser tratada com um texto deletado.</del></p>
<p><s>Esta linha de texto deve ser tratada como algo impreciso.</s></p>
<p><ins>Esta linha de texto deve ser tratada como uma adição ao documento.</ins></p>
<p><u>Esta linha de texto vai ser renderizada com underline.</u></p>
<p><small>Esta linha deve ser usada para letras com tamanhos menores.</small></p>
<p><strong>Esta linha será renderiza em negrito.</strong></p>
<p><em>Esta linha será renderiza em itálico.</em></p>
```

## Utilitários de texto

Você pode alterar a espessura do texto, fonte, tamanho, e cores com nossas classes modificadoras para _**textos**_ e _**cores**_.

## Abreviações

Nós oferecemos uma implementação customizada para o `<abbr>`, elemento de abreviações do HTML, ele revela o texto completo ao passar o cursor sobre o mesmo. Nós aplicamos um sublinhado especial juntamente com um cursor de ajuda para oferecer um feedback visual adicional.  

Você pode usar a classe `.nv-initialism` para aplicar um tamanho de fonte menor.  

```html
<p><abbr title="attribute">attr</abbr></p>
<p><abbr title="HyperText Markup Language" class="initialism">HTML</abbr></p>
```

## Blocos de citações

Você pode criar blocos de conteúdo de outras fontes em seu documento utilizando a tag blockquote juntamente com a classe `.nv-blockquote`.  

```html
<blockquote class="nv-blockquote">
  <p class="nv-mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
</blockquote>
```

## Nomeando uma fonte

Use a tag footer juntamente com a classe nv-blockquote-footer  para identificar uma fonte. Envolva o nome da fonte usando a tag `<cite>`.  

```html
<blockquote class="nv-blockquote">
  <p class="nv-mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer class="nv-blockquote-footer">Alguém famoso em <cite title="Source Title">Título da página.</cite></footer>
</blockquote>
``` 
