---
description: Os ícones podem significar ações, produtos e conceitos. Ao oferecer uma abstração visual compacta ultrapassamos os limites dos idiomas e conservamos espaço na tela.
title: Ícones no Nuiverse UI.
nnp.date: 11/22/2020
nnp.topic: article
keywords: icones, ícones, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# _Ícones_

Os ícones podem significar ações, produtos e conceitos. Ao oferecer uma abstração visual compacta ultrapassamos os limites dos idiomas e conservamos espaço na tela. Veja mais abaixo como e quando usar os ícones do Nuiverse UI.
Ícones podem ser utilizados dentro e fora de aplicativos e além de páginas e aplicações Web.  

Em um aplicativo podemos utilizar ícones para representar visualmente ações como copiar, colar, salvar etc...  


Sistemas operacionais, navegadores e serviços similares utilizam ícones para representar o seu produto, seja ele um WebApp, jogo ou aplicativo.  


## Quando usar ícones?

Utilize ícones para representar ações como copiar ou colar, e procure utilizar ícones que existam para o conceito que você quer representar.  

Nós fornecemos mais de 200 ícones em forma da fonte Nuiverse Icons, e o Nuiverse possui componentes prontos que utilizam nossa família de ícones criando um sistema mais integrado e familiar. Porém caso você deseja utilizar outro pacote de fontes, veja nosso **guia**.
