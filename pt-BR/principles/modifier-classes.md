---
description: Entenda nossa nomeclatura de classes modificadoras e construa aplicações incríveis.
title: Classes modificadoras do Nuiverse UI.
nnp.date: 11/22/2020
nnp.topic: article
keywords: classes, modificadoras, nomeclaturas, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# _Classes modificadoras_

O Nuiverse UI oferece diversas classes modificadoras em seu núcleo para facilitar a personalização de elementos e a construção da interface. As classes podem ser utilizadas em conjunto sem qualquer limite de combinação.  

## Nomes das classes

Todas as classes do Nuiverse UI possuem o prefixo **“nv-“**, oferecemos este prefixo a fim de evitar colisões com outras classes e como uma maneira de criar uma padrão em nosso sistema.
Outro fator importante é que nós abreviamos o nome de algumas classes, para evitar que fiquem muito grandes. Por exemplo quando o nome da função oferecer mais de duas palavras, nós abreviaremos a primeira palavra, seguindo essa formula:  

`prefixo-modificador-valor = nv-d-relative.`  

No exemplo acima, o prefixo do Nuiverse `nv-` acompanha a classe, seguido do modificador `d`, que significa `display` e o seu valor `relative`, essa formula se aplica a todos os modificadores, você poderá consulta-los verificando a página individual de cada classe.  

Nós oferecemos um número alto de modificadores e é possível alcançar o resultado desejado na maioria das vezes utilizando apenas as classes que oferecemos, encorajamos fortemente para sempre utilize as classes modificadoras do Nuiverse UI, isso evita ter que escrever código CSS personalizado e ajuda a criar mais consistência entre as plataformas e projetos.
