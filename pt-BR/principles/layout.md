---
description: Entenda os princípios usados por nós na construção de layouts e crie layouts incríveis.
title: Layout do Nuiverse UI.
nnp.date: 11/22/2020
nnp.topic: article
keywords: layout, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# _Layout_

O Nuiverse UI usa um sistema de grade de 8 pontos para  alinhar, espaçar e distribuir os componentes, criando um sistema harmônico e padronizado. Isso significa que margens, paddings, altura de botões e vários outros recursos serão sempre **múltiplos de 8 pixels**. Isso facilita a composição dos componentes do Nuiverse UI.  

## Uso da grade de 8 pontos

Nós utilizamos o sistema de grade de 8 pontos para diferentes casos de usos, ele está presente nas ilustrações, ícones e sistemas de grid do Nuiverse UI.  

## Componentes

Todos os componentes do Nuiverse possuem alturas, margens e margens internas (paddings) alinhados com a grade de 8 pontos. Isso significa que por exemplo um elemento `<form>` possui margem interna de múltiplo de 8 e seus elementos também são alinhados seguindo esse mesmo sistema. 

## Grid

Nosso sistema de grid é construído baseado na grade de 8 pontos, ele fornece mais controle e pode ser usado para construir uma UI mais avançada. Ela pode ser usada para criar interfaces de usuário onde o conteúdo necessita de mais controle e desta forma, aplicar o conceito da grade de 8 pontos.  

## Margens e espaçamentos

As margens e espaçamentos também seguem a grade de 8 pontos, você pode utilizar nossos modificadores para adicionar espaços ou margens compatíveis com a grade de 8 pontos. Por exemplo o modificador de margem `.nv-mt-1`, adicionará uma margem no topo do elemento de 8px, já a `.nv-mt-2` uma margem de 16px, e assim por diante.  
