---
description: Entenda como funciona o sistema de cores do Nuiverse UI e construa uma interface rica.
title: Sistema de cores do Nuiverse UI.
nnp.date: 11/22/2020
nnp.topic: article
keywords: cores, nuiverse ui, js, ts, web, components, stencil
nnp.contentlocale: pt-BR
---

# Cor

A cor é uma maneira intuitiva e prática de comunicar informações ao usuário, ela pode ser utilizada para indicar o status de determinada função, enviar comentários, oferecer feedbacks e continuidade.  

No Nuiverse UI nós temos uma paleta de cores onde tudo começa com a cor primária, desta forma nós geramos cores mais escuras e claras para preencher as diferentes áreas dos componentes.  

Ela pode ser facilmente personalizável, e cada um de nossos produtos possui uma cor diferente, entretanto você é livre para estende-lo e adicionar a sua cor principal.

## Princípios das cores 

**A cor é relevante**

Quando utilizada de maneira equilibrada para realçar os componentes da interface do usuário, ela pode criar uma interface limpa, coesa e de fácil compreensão.

**Interatividade**

Utilize cores para realçar elementos interativos em sua aplicação, através delas dizemos ao usuário de maneira simples quais elementos são interativos. Por exemplo é muito comum sites usarem azul para links externos.

## Temas

O Nuiverse oferece temas claros e escuros e você é livre para escolher o que lhe agrada ou oferecer opções de customização para seus usuários. De maneira geral os temas do Nuiverse afetam fundos das páginas, textos e os componentes interativos, além do sistema de cores.

## Adaptação inteligente

Os componentes do Nuiverse são construídos para serem responsivos, flexíveis e unidos, com isso em mente, nós construímos um sistema automatizado e integrado para que os componentes se adaptem ao sistema de cores. Veja por exemplo abaixo como o componente **Select** se comporta com as mudanças de cores.

## Customização de temas

O Nuiverse possui uma classe global onde as cores são especificadas, ao alterar as cores do seu aplicativo e compilar nosso arquivo sass, o sistema irá cuidar para que as cores sejam aplicadas de maneira fácil.  

Existe um plano para suportamos a criação de temas personalizadas em produção.  

## Cor de destaque

Os componentes do Nuiverse UI utilizam a cor de destaque (primária), para aplicar cores aos componentes interativos. Essa paleta de cores é fornecida juntamente com o Nuiverse, entretanto você pode personaliza-la alterando o root de cores e compilando o sass.

## Paleta de cores
Um Mixin de cores gera as diferentes tonalidades de cores presentes no Nuiverse.  

Você pode utilizar essas cores pelas classes:  

- `.nv-system-color-light-3`
- `.nv-system-color-light-2`
- `.nv-system-color-light-1`
- `.nv-system-color-dark-3`
- `.nv-system-color-light-2`
- `.nv-system-color-light-1`

Você também pode acessar a paleta de cores através de nossa API JS e aplica-lo a todo o sistema:
<!-- //TODO verificar o que sera feito aqui -->
```js
// Retornar um Array de objetos com as cores dark/light
const nv = new Nuiverse();
nv.getColorValue(); 
```

